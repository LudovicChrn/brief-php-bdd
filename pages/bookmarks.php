<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Base de données</title>
    <script src="https://kit.fontawesome.com/8334dc67da.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
    <link rel="shortcut icon" href="../icon.png" type="image/x-icon">
</head>
<body>

    <header>
        <nav class="breadcrumb">
            
            <ul>
                <li class="first"><a href="../index.php">Accueil</a></li>
                <li>Database</li>
            </ul>            
        </nav>

        <div class="header_title">
            <h2>Page de la BDD</h2>
        </div>
    </header>
    <div class="sidenav">
        <div class="sidenav-content">
            <div class="img">
                <img src="../603156.png" alt=""><p>PHP BDD</p>
            </div>
        <a href="../index.php">Accueil</a>
        <a href="./bookmarks.php">Base de données</a>         
        </div>

    </div>

<div class="bookmark-section">
    <div class="link-section">
        <h1>Les relations</h1>

            <table>
                    <tr>
                        <th>Favoris</th>
                        <th>Categorie</th>
                        <th>Modifier</th>
                    </tr>
                <?php
                    require('pdo.php');

                    $sth = $bdd->prepare('SELECT l.id as "ID Fav", l.nom as "Nom Favoris", l.url as "Lien Favoris", c.id as "ID Categ", lc.id as "LinkCate ID", GROUP_CONCAT(c.nom SEPARATOR " / ") as "Nom Categorie"
                    FROM link as l
                    left join link_category as lc ON l.id = lc.link_id
                    left join category as c ON c.id = lc.category_id
                    GROUP BY l.id');
                    $sth->execute();
                    $resultats = $sth->fetchAll(PDO::FETCH_ASSOC);
                    
                    foreach($resultats as $resultat){
                        printf('<tr><td><a href="' . $resultat["Lien Favoris"] . '" target="_blank">' . $resultat["Nom Favoris"] . '</a></td>');
                        if ($resultat["Nom Categorie"] === NULL && $resultat["LinkCate ID"] === NULL){
                            printf('<td>Sans catégorie</td>');
                            printf('<td><i class="fas fa-times"></i></td></tr>');
                        } else {
                            printf('<td><p class="with_categ">' . $resultat["Nom Categorie"] . '</p></td>');
                            printf('<td><a href="../pages/edit_datas/edit_link_cate.php?id_link=' . $resultat["ID Fav"] . '">' . '<i class="fas fa-edit"></i>' . '</a></td></tr>');
                        }
                        
                    }

                    $sth->closeCursor(); // Termine le traitement de la requête

                    ?>
            </table>
            <?php 
            
            printf('<a href="./add_datas/add_link.php" class="add">Ajouter un lien</a>');
            printf('<a href="./add_datas/add_category.php" class="add">Ajouter une catégorie</a>');
            
            ?>
    </div>
</div>
    
</body>
</html>