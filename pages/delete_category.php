<?php

include "pdo.php"; // Using database connection file here

$id = $_GET['id']; // get id through query string

$del = "DELETE FROM category WHERE id= :id";
$stmt = $bdd->prepare($del);
$stmt->bindValue(':id', $id);
$stmt->execute();

if ($stmt){
    $bdd = NULL; // Close connection
    header("Location: ../pages/add_datas/add_category.php"); // redirects to all records page
    exit;
} else {
    echo "Error deleting record"; // display error message if not delete
}
?>