<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajouter une catégorie</title>
    <link rel="stylesheet" href="../style.css">
    <script src="https://kit.fontawesome.com/8334dc67da.js" crossorigin="anonymous"></script>
    <link rel="shortcut icon" href="../../icon.png" type="image/x-icon">
</head>
<body>

    <header>
        <nav class="breadcrumb">
            <ul>
                <li class="first"><a href="../../index.php">Accueil</a></li>
                <li>Ajouter catégorie</li>
            </ul>            
        </nav>

        <div class="header_title">
            <h2>Page de création de catégories</h2>
        </div>
    </header>
    <div class="sidenav">
        <div class="sidenav-content">
            <div class="img">
                <img src="../../603156.png" alt=""><p>PHP BDD</p>
            </div>
        <a href="../../index.php">Accueil</a>
        <a href="../bookmarks.php">Base de données</a>
        </div>

    </div>

    <?php
        require('../pdo.php');

            // if(isset($_POST['submit2'])){

            //     header("Location: ../add_datas/add_category.php");

            // }

        ?>
    <div class="second-section">
        <div class="link-section">
            <h2>Les catégories (<?php 
            $count = $bdd->prepare('SELECT * FROM category');
            $count->execute();

            $counter = $count->rowCount();
            print_r($counter);
            
            ?>)</h2>

            <table>
                    <tr>
                        <th>ID</th>
                        <th>Catégories</th>
                        <th>Modifier</th>
                        <th>Supprimer</th>
                    </tr>
                <?php

                    $reponse2 = $bdd->prepare('SELECT c.id as "ID Cat", c.nom as "Nom Categorie" FROM category as c');
                    $reponse2->execute();
                    $resultats2 = $reponse2->fetchAll(PDO::FETCH_ASSOC);

                    foreach($resultats2 as $donnees2){
                ?>
                        <tr><td> <?php printf($donnees2["ID Cat"]); ?></td>
                        <td><?php printf($donnees2["Nom Categorie"]); ?></td>
                        <td><a href="../edit_datas/edit_category.php?id=<?php printf($donnees2["ID Cat"]); ?>"><i class="fas fa-edit"></i></a></td>
                        <td><a href="../delete_category.php?id=<?php printf($donnees2["ID Cat"]); ?>"><i class="fas fa-trash"></i></a></td></tr>
                    
                <?php    }

                    $reponse2->closeCursor(); // Termine le traitement de la requête

                    ?>
            </table>
        </div>
        
        <h3>Ajouter une catégorie.</h3>
        <form action="" method="post">
            <label for="cat-name">Nom de la catégorie</label>
            <input type="text" name="cat-name" required>

            <input type="submit" name="submit2" value="Envoyer">
        </form>
        <?php
            if(isset($_POST['submit2'])){

                $catname = htmlspecialchars($_POST['cat-name']);
                $cattable = $bdd->prepare('SELECT nom FROM category WHERE nom=?');
                $cattable->execute([$catname]);
                $catname2 = $cattable->fetch();

                if ($catname2){
                    printf('La donnée existe déjà');
                } else {
                    $sql2 = 'INSERT INTO category(nom) VALUES(:catname)';
                    $res2 = $bdd->prepare($sql2);
                    $exec2 = $res2->execute(array(":catname"=>$catname));                
                }

            }
        ?>
    </div>
</body>
</html>