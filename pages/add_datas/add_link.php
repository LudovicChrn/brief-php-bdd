<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajouter un lien</title>
    <script src="https://kit.fontawesome.com/8334dc67da.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../style.css">
    <link rel="shortcut icon" href="../../icon.png" type="image/x-icon">
</head>
<body>

    <header>
        <nav class="breadcrumb">
            <ul>
                <li class="first"><a href="../../index.php">Accueil</a></li>
                <li>Ajouter liens</li>
            </ul>            
        </nav>

        <div class="header_title">
            <h2>Page de création de liens</h2>
        </div>
        <div class="header_invisible">

        </div>
    </header>
    <div class="sidenav">
        <div class="sidenav-content">
            <div class="img">
                <img src="../../603156.png" alt=""><p>PHP BDD</p>
            </div>
        <a href="../../index.php">Accueil</a>
        <a href="../bookmarks.php">Base de données</a>
        </div>

    </div>

    <?php
        require('../pdo.php');

        // if(isset($_POST['submit'])){

        //     header("Location: ../add_datas/add_link.php");
        // }

        ?>
<div class="second-section">
        <div class="link-section">
    <h2>Les liens (<?php 
            $count = $bdd->prepare('SELECT * FROM link');
            $count->execute();

            $counter = $count->rowCount();
            print_r($counter);
            
            ?>)</h2>

        <table>
                <tr>
                    <th>ID</th>
                    <th>Favoris</th>
                    <th>Modifier</th>
                    <th>Supprimer</th>
                </tr>
            <?php
                $reponses2 = $bdd->prepare('SELECT l.id as "ID Fav", l.nom as "Nom Favoris", l.url as "Lien Favoris" FROM link as l');
                $reponses2->execute();
                $resultats2 = $reponses2->fetchAll(PDO::FETCH_ASSOC);

                foreach($resultats2 as $donnees){
            ?>
                    <tr><td><?php printf($donnees["ID Fav"]); ?></td>
                    <td><a href="<?php printf($donnees["Lien Favoris"]); ?>" target="_blank"><?php printf($donnees["Nom Favoris"]); ?></a></td>
                    <td><a href="../edit_datas/edit_link.php?id=<?php printf($donnees["ID Fav"]); ?>"><i class="fas fa-edit"></i></a></td>
                    <td><a href="../delete_link.php?id=<?php printf($donnees["ID Fav"]); ?>"><i class="fas fa-trash"></i></a></td></tr>
            <?php
                }
            
                $reponses2->closeCursor(); // Termine le traitement de la requête
                ?>
        </table>
    <h3>Ajouter un lien.</h3>
        <form action="" method="post">
            <label for="link-name">Nom du lien</label>
            <input type="text" name="link-name" required>
            <label for="link-url">URL du lien</label>
            <input type="url" name="link-url" placeholder="https://www." pattern="https://www.*" required>
            <label for="categories">Categories</label>
            <p class="create-cate">Votre catégorie n'existe pas? <a href="./add_category.php">créée là!</a></p>
            <select name="categories[]" multiple>
                <option value="">Pas de catégorie</option>
                <?php
                    $reponse = $bdd->prepare('SELECT * FROM category');
                    $reponse->execute();
                    $resultats = $reponse->fetchAll(PDO::FETCH_ASSOC);
                    
                    foreach($resultats as $resultat){
                        printf('<option value="' . $resultat['id'] . '">' . $resultat['nom'] . '</option>');
                    }
                ?>
            </select>

            <input type="submit" name="submit" value="Envoyer">
        </form>
                <?php 
                        if(isset($_POST['submit'])){

                            $linkname = htmlspecialchars($_POST['link-name']);
                            $linkurl = $_POST['link-url'];
                            $categ_ids = $_POST["categories"];
                
                            $nametable = $bdd->prepare('SELECT nom FROM link WHERE nom=?');
                            $nametable->execute([$linkname]);
                            $nametable2 = $nametable->fetch();
                
                            if ($nametable2){
                                printf('Ce nom existe déjà');
                            } else {
                                $sql = 'INSERT INTO link(nom, url) VALUES(:linkname, :linkurl)';
                                $res = $bdd->prepare($sql);
                                $exec = $res->execute(array(":linkname"=>$linkname,":linkurl"=>$linkurl));
                
                                if ($exec){
                                    $lastid = $bdd->lastInsertId();
                
                                    $test = 'INSERT INTO link_category(link_id, category_id) VALUES(:linkid, :categoryid)';
                                    $restest = $bdd->prepare($test);
                                    foreach ($categ_ids as $categ_id){
                                        $exectest = $restest->execute(array(":linkid"=>$lastid, ":categoryid"=>$categ_id));
                                    }
                                } else {
                                    printf('Echec de l\'opération d\'insertion');
                                };
                            }
                        }
                        ?>
        </div>
</div>
</body>
</html>