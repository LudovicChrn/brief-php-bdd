<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modifier une catégorie</title>
    <link rel="stylesheet" href="../style.css">
    <link rel="shortcut icon" href="../../icon.png" type="image/x-icon">
</head>
<body>

    <header>
        <nav class="breadcrumb">
            <ul>
                <li class="first"><a href="../../index.php">Accueil</a></li>
                <li><a href="../add_datas/add_category.php">Ajouter catégorie</a></li>
                <li>Modifier catégorie</li>
            </ul>            
        </nav>

        <div class="header_title">
            <h2>Page pour modifier une catégorie</h2>
        </div>
        <div class="header_invisible">

        </div>
    </header>
    <div class="sidenav">
        <div class="sidenav-content">
            <div class="img">
                <img src="../../603156.png" alt=""><p>PHP BDD</p>
            </div>
        <a href="../../index.php">Accueil</a>
        <a href="../bookmarks.php">Base de données</a>
        </div>

    </div>


    <div class="second-section">
        <h1>Modifier catégorie.</h1>
    <?php
        require('../pdo.php');

        $id = $_GET["id"];

        $sql = "SELECT * FROM category WHERE id= :id";
        $res = $bdd->prepare($sql);
        $res->execute(array(":id"=>$id));
        $data = $res->fetch(PDO::FETCH_ASSOC);

        if(isset($_POST['submit2'])){

            $catname = htmlspecialchars($_POST['link-name']);

            $sql2 = 'UPDATE category SET nom = :catname WHERE id =:id';
            $res2 = $bdd->prepare($sql2);
            $exec2 = $res2->execute(array(':catname' => $catname, ':id' => $id));

            if ($exec2){
                printf( '<h2>Données modifiées</h2>');
            } else if (strpos($exec2, '<script>') == true){
                printf('Vous ne pouvez pas faire ceci');
            } else {
                printf( 'Echec de l\'opération de modification');
            };

        }
    ?>

        <form action="" method="post">
            <label for="link-name">Nom de la catégorie</label>
            <input type="text" name="link-name" value="<?php printf("%s", $data['nom']); ?>" required>

            <input type="submit" name="submit2" value="Modifier">
        </form>
    </div>
</body>
</html>