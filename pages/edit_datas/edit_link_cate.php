<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modifier relation</title>
    <link rel="stylesheet" href="../style.css">
    <link rel="shortcut icon" href="../../icon.png" type="image/x-icon">
</head>
<body>

    <header>
        <nav class="breadcrumb">
            <ul>
                <li class="first"><a href="../bookmarks.php">Database</a></li>
                <li>Modification</li>
            </ul>            
        </nav>

        <div class="header_title">
            <h2>Page pour modifier une données</h2>
        </div>
        <div class="header_invisible">

        </div>
    </header>
    <div class="sidenav">
        <div class="test">
        <a href="../../index.php">Accueil</a>
        <a href="../bookmarks.php">Base de données</a>
        </div>

    </div>


    <div class="second-section">
    <h1>Modifier relation.</h1>
    <?php
        require('../pdo.php');

        $id_fav = $_GET["id_link"];

        $sql = "SELECT link_category.category_id FROM link_category INNER JOIN link ON link_category.link_id = link.id WHERE link.id = :id";
        $res = $bdd->prepare($sql);
        $res->execute(array(":id"=>$id_fav));
        $datas = $res->fetchAll(PDO::FETCH_ASSOC);

        $sth2 = 'SELECT l.nom as "Nom Favoris", l.url as "Lien Favoris" FROM link as l WHERE id = :id_link';
        $res2 = $bdd->prepare($sth2);
        $res2->execute(array(":id_link"=>$id_fav));
        $data2 = $res2->fetch(PDO::FETCH_ASSOC);
        
    ?>

        <form action="" method="post">
            <label for="link-name">Nom du lien</label>
            <input type="text" name="link-name" value="<?php echo $data2['Nom Favoris'] ?>" required>
            <label for="link-url">URL du lien</label>
            <input type="url" name="link-url" value="<?php echo $data2['Lien Favoris'] ?>" required>
            <label for="categories">Categories</label>
            <select name="categories[]" multiple>
                <?php
                    $reponses = $bdd->prepare('SELECT * FROM category');
                    $reponses->execute();
                    $resultats2 = $reponses->fetchAll(PDO::FETCH_ASSOC);
                    foreach($resultats2 as $resultat2){
                        printf('<option value="' . $resultat2['id'] . '"');
                            foreach($datas as $key=>$data){
                                if ($data['category_id'] === $resultat2['id']) {
                                    printf( 'selected="selected"');
                                }
                            }                            
                            printf( '>' . $resultat2['nom'] . '</option>');
                    }
                ?>
            </select>

            <input type="submit" name="submit" value="Modifier">
            <?php
                    if(isset($_POST['submit'])){

                        $linkname = htmlspecialchars($_POST['link-name']);
                        $linkurl = $_POST['link-url'];
                        $categ_ids = $_POST["categories"];

                        // UPDATE link_category SET link_category.category_id = 41, link_category.link_id = (SELECT id FROM link WHERE id = 97) WHERE id = 45

                        $edit = 'UPDATE link SET link.nom = :linkname, link.url = :linkurl WHERE id = :id_fav';
                        $res = $bdd->prepare($edit);
                        $exec = $res->execute(array(':linkname'=> $linkname, ':linkurl'=> $linkurl, ':id_fav'=>$id_fav));

                        $upd = 'DELETE FROM link_category WHERE link_id = :id_favs';
                        $resupd = $bdd->prepare($upd);
                        $execupd = $resupd->execute(array(':id_favs'=>$id_fav));
                        
                        foreach ($categ_ids as $categ_id){
                            $ins = 'INSERT INTO link_category(link_id, category_id) VALUES(:linkid, :cateid)';
                            $resins = $bdd->prepare($ins);
                            $execins = $resins->execute(array(':linkid' => $id_fav ,':cateid' => $categ_id));
                        }
                        header("Location: ../bookmarks.php");
                    }
                ?>
        </form>
    </div>
</body>
</html>