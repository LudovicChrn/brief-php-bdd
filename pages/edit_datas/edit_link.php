<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modifier un lien</title>
    <link rel="stylesheet" href="../style.css">
    <link rel="shortcut icon" href="../../icon.png" type="image/x-icon">
</head>
<body>

    <header>
        <nav class="breadcrumb">
            <ul>
                <li class="first"><a href="../../index.php">Accueil</a></li>
                <li><a href="../add_datas/add_link.php">Ajouter liens</a></li>
                <li>Modifier lien</li>
            </ul>            
        </nav>

        <div class="header_title">
            <h2>Page pour modifier un lien</h2>
        </div>
        <div class="header_invisible">

        </div>
    </header>
    <div class="sidenav">
        <div class="sidenav-content">
            <div class="img">
                <img src="../../603156.png" alt=""><p>PHP BDD</p>
            </div>
        <a href="../../index.php">Accueil</a>
        <a href="../bookmarks.php">Base de données</a>
        </div>

    </div>


    <div class="second-section">
    <h1>Modifier lien.</h1>
    <?php
        require('../pdo.php');

        $id = $_GET["id"];

        $sql = "SELECT * FROM link WHERE id= :id";
        $res = $bdd->prepare($sql);
        $res->execute(array(":id"=>$id));
        $data = $res->fetch(PDO::FETCH_ASSOC);
    ?>

        <form action="" method="post">
            <label for="link-name">Nom du lien</label>
            <input type="text" name="link-name" value="<?php echo $data['nom'] ?>" required>
            <label for="link-url">URL du lien</label>
            <input type="url" name="link-url" value="<?php echo $data['url'] ?>" required>

            <input type="submit" name="submit" value="Modifier">
            <?php
                    if(isset($_POST['submit'])){

                        $linkname = htmlspecialchars($_POST['link-name']);
                        $linkurl = $_POST['link-url'];

                        $sql = 'UPDATE link SET link.nom = :linkname, link.url = :linkurl WHERE link.id =:id';
                        $res = $bdd->prepare($sql);
                        $exec = $res->execute(array(':linkname' => $linkname, ':linkurl' => $linkurl, ':id' => $id));

                        header("Location: ../add_datas/add_link.php");
                    }
                ?>
        </form>
    </div>
</body>
</html>